package com.valentin.go4lunch.database;

import android.content.Context;
import android.util.Log;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

import com.valentin.go4lunch.database.dao.UserDao;
import com.valentin.go4lunch.models.User;

@Database(entities = User.class, version = 1, exportSchema = false)
public abstract class Go4LunchDatabase extends RoomDatabase {

    // SINGLETON
    private static volatile Go4LunchDatabase INSTANCE;

    // --- INSTANCE ---
    public static Go4LunchDatabase getInstance(Context context) {
        if (INSTANCE == null) {
            synchronized (Go4LunchDatabase.class) {
                if (INSTANCE == null) {
                    INSTANCE = Room.databaseBuilder(context.getApplicationContext(),
                            Go4LunchDatabase.class, "MyDatabase.db")
                            .allowMainThreadQueries()
                            .build();
                }
            }
        }
        return INSTANCE;
    }

    public abstract UserDao userDao();

}
