package com.valentin.go4lunch.database.dao;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;

import com.valentin.go4lunch.models.User;

import java.util.List;

@Dao
public interface UserDao {
    @Insert
    void insertUser(User user);

    @Query("SELECT * FROM users WHERE uid = :uid")
    User getUserWithUid(String uid);

    @Query("DELETE FROM users WHERE uid = :uid")
    void deleteUser(String uid);
}
