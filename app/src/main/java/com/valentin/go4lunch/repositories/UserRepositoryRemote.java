package com.valentin.go4lunch.repositories;

import android.util.Log;

import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.valentin.go4lunch.models.User;
import com.valentin.go4lunch.utils.UserHelper;

public class UserRepositoryRemote implements UserRepositoryContrat{

    private static final String LOG = UserRepositoryRemote.class.getSimpleName();
    private final FirebaseFirestore remoteDB = FirebaseFirestore.getInstance();

    @Override
    public void createUser(User user) {
        remoteDB.collection("users").document(user.getUid()).set(user);
    }

    public LiveData<User> getCurrentUser(String uid) {

        Log.d(LOG, "[LOG] --> Récupération de l'utilisateur " + uid + " via FIRESTORE !");
        final MutableLiveData<User> userLiveData = new MutableLiveData<>();
        UserHelper.getUser(uid)
                .addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                        if(task.isSuccessful()) {
                            DocumentSnapshot document = task.getResult();
                            if(document != null && document.exists()) {
                                User user = document.toObject(User.class);
                                userLiveData.setValue(user);
                            }
                        }
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.e(LOG, "[LOG] --> " + e);
                    }
                });

        return userLiveData;
    }

}
