package com.valentin.go4lunch.repositories;

import androidx.lifecycle.LiveData;

import com.valentin.go4lunch.models.User;

public interface UserRepositoryContrat {
    public void createUser(User user);
    public LiveData<User> getCurrentUser(String uid);
}
