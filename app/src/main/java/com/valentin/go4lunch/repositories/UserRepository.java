package com.valentin.go4lunch.repositories;

import android.content.Context;
import android.util.Log;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.valentin.go4lunch.database.Go4LunchDatabase;
import com.valentin.go4lunch.models.User;

public class UserRepository implements UserRepositoryContrat{

    private static final String LOG = UserRepository.class.getSimpleName();
    private final Go4LunchDatabase localDB;

    public UserRepository(Context context) {
        localDB = Go4LunchDatabase.getInstance(context);
    }

    @Override
    public void createUser(User user) {
        if(localDB.userDao().getUserWithUid(user.getUid()) == null) {
            localDB.userDao().insertUser(user);
        }
    }

    @Override
    public LiveData<User> getCurrentUser(String uid) {
        Log.d(LOG, "[LOG] --> Récupération de l'utilisateur " + uid + " via ROOM !");

        final MutableLiveData<User> userLiveData = new MutableLiveData<>();
        userLiveData.setValue(localDB.userDao().getUserWithUid(uid));
        return userLiveData;
    }
}
