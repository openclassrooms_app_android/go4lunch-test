package com.valentin.go4lunch.viewmodels;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.valentin.go4lunch.models.User;
import com.valentin.go4lunch.services.UserService;

import java.util.concurrent.Executor;

public class UserViewModel extends ViewModel {

    private static final String LOG = UserViewModel.class.getSimpleName();
    private final UserService userService;
    private final Executor executor;
    private final MutableLiveData<User> userLiveData = new MutableLiveData<>();

    public UserViewModel(UserService userService, Executor executor) {
        this.userService = userService;
        this.executor = executor;
    }

    public LiveData<User> getCurrentUser(String uid) {
        return userService.getCurrentUser(uid);
    }

    public void createUser(User user) {
        executor.execute(() -> {
            userService.createUser(user);
        });
    }

}
