package com.valentin.go4lunch.services;

import androidx.lifecycle.LiveData;

import com.valentin.go4lunch.models.User;

public interface UserServiceContrat {
    public void createUser(User user);
    public LiveData<User> getCurrentUser(String uid);
}
