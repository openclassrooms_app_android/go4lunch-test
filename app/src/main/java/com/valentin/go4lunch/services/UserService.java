package com.valentin.go4lunch.services;

import android.content.Context;
import android.util.Log;

import androidx.lifecycle.LiveData;

import com.valentin.go4lunch.models.User;
import com.valentin.go4lunch.repositories.UserRepository;
import com.valentin.go4lunch.repositories.UserRepositoryRemote;

public class UserService implements UserServiceContrat{

    private static final String LOG = UserService.class.getSimpleName();

    private final UserRepository repoLocal;
    private final UserRepositoryRemote repoRemote;

    private final Context context;
    private static UserService INSTANCE = null;


    private UserService(Context context) {
        this.context = context;

        repoLocal = new UserRepository(context);
        repoRemote = new UserRepositoryRemote();
    }

    // SINGLETON
    public synchronized static UserService getInstance(Context context) {
        if(INSTANCE == null) {
            INSTANCE = new UserService(context);
        }
        return INSTANCE;
    }

    @Override
    public void createUser(User user) {
        repoRemote.createUser(user);
        Log.d("UserService", "[LOG] --> Création de l'utilisateur via FIRESTORE !");
        repoLocal.createUser(user);
        Log.d("UserService", "[LOG] --> Création de l'utilisateur via ROOM !");
    }

    public LiveData<User> getCurrentUser(String uid) {
        LiveData<User> currentUser = null;
        currentUser = repoLocal.getCurrentUser(uid);
        return currentUser;
    }
}
