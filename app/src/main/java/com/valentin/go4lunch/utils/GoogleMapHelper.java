package com.valentin.go4lunch.utils;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.util.Log;

import androidx.annotation.IdRes;
import androidx.core.content.ContextCompat;

import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.Task;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.api.model.PlaceLikelihood;
import com.google.android.libraries.places.api.net.FindCurrentPlaceRequest;
import com.google.android.libraries.places.api.net.FindCurrentPlaceResponse;
import com.google.android.libraries.places.api.net.PlacesClient;
import com.valentin.go4lunch.R;

import java.util.Arrays;
import java.util.List;

public class GoogleMapHelper {

    private final String LOG = GoogleMapHelper.class.getSimpleName();
    private final float DEFAULT_ZOOM = 17f;

    private final Context activityContext;
    private final Resources resources;

    public GoogleMapHelper(Context activityContext, Resources resources) {
        this.activityContext = activityContext;
        this.resources = resources;
    }

    public BitmapDescriptor bitmapFromVector(Context context, int vectorResId) {
        // below line is use to generate a drawable.
        Drawable vectorDrawable = ContextCompat.getDrawable(context, vectorResId);

        // below line is use to set bounds to our vector drawable.
        vectorDrawable.setBounds(0, 0, vectorDrawable.getIntrinsicWidth(), vectorDrawable.getIntrinsicHeight());

        // below line is use to create a bitmap for our
        // drawable which we have added.
        Bitmap bitmap = Bitmap.createBitmap(vectorDrawable.getIntrinsicWidth(), vectorDrawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);

        // below line is use to add bitmap in our canvas.
        Canvas canvas = new Canvas(bitmap);

        // below line is use to draw our
        // vector drawable in canvas.
        vectorDrawable.draw(canvas);

        // after generating our bitmap we are returning our bitmap.
        return BitmapDescriptorFactory.fromBitmap(bitmap);
    }

    public Bitmap resizeBitmap(@IdRes int drawableInt, int width, int height){
        Bitmap imageBitmap = BitmapFactory.decodeResource(resources, drawableInt);
        return Bitmap.createScaledBitmap(imageBitmap, width, height, false);
    }

    public boolean checkLocationPermission() {
        return (ContextCompat.checkSelfPermission(activityContext, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED);
    }

    public interface OnGetMap {
        void onMarkersCallback(GoogleMap map);
        void onLocationCallback(GoogleMap map, Location location, float zoom);
    }

    public void setMap(SupportMapFragment supportMapFragment, OnGetMap onGetMap) {
        if(supportMapFragment != null) {
            supportMapFragment.getMapAsync(googleMap -> {

                if(checkLocationPermission()) {

                    // Set new map style
                    googleMap.setMapStyle(MapStyleOptions.loadRawResourceStyle(
                            activityContext.getApplicationContext(), R.raw.standard_map_json));

                    // Display my current location
                    googleMap.setMyLocationEnabled(true);

                    // Hide location button for zoom in on my current position
                    googleMap.getUiSettings().setMyLocationButtonEnabled(false);

                    /*
                     * Map loaded listener
                     * - Animate camera on my current position
                     */
                    googleMap.setOnMapLoadedCallback(() -> {

                        onGetMap.onMarkersCallback(googleMap);

                        FusedLocationProviderClient fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(activityContext.getApplicationContext());
                        fusedLocationProviderClient.getLastLocation()
                                .addOnSuccessListener(location -> {
                                    googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(location.getLatitude(), location.getLongitude()), DEFAULT_ZOOM), 1000, null);
                                    onGetMap.onLocationCallback(googleMap, location, DEFAULT_ZOOM);
                                })
                                .addOnFailureListener(e -> Log.e(LOG, "[LOG] --> " + e));
                    });
                }

            });
        }
    }

    public void setMarkers(GoogleMap map, int drawableInt, String apiKey) {
        Places.initialize(activityContext.getApplicationContext(), apiKey);
        PlacesClient placesClient = Places.createClient(activityContext.getApplicationContext());

        // Use fields to define the data types to return.
        List<Place.Field> placeFields = Arrays.asList(Place.Field.ID, Place.Field.NAME, Place.Field.LAT_LNG);

        // Use the builder to create a FindCurrentPlaceRequest.
        FindCurrentPlaceRequest request = FindCurrentPlaceRequest.newInstance(placeFields);

        if(checkLocationPermission()) {
            Task<FindCurrentPlaceResponse> placeResponse = placesClient.findCurrentPlace(request);
            placeResponse.addOnCompleteListener(task -> {
                if (task.isSuccessful()){
                    FindCurrentPlaceResponse response = task.getResult();
                    for (PlaceLikelihood placeLikelihood : response.getPlaceLikelihoods()) {
                        if(placeLikelihood.getPlace().getLatLng() != null) {

                            if(drawableInt != 0) {
                                BitmapDescriptor marker = BitmapDescriptorFactory.fromBitmap(this.resizeBitmap(drawableInt,90,90));
                                map.addMarker(new MarkerOptions().position(placeLikelihood.getPlace().getLatLng()).title(placeLikelihood.getPlace().getName()).icon(marker));
                            } else {
                                map.addMarker(new MarkerOptions().position(placeLikelihood.getPlace().getLatLng()).title(placeLikelihood.getPlace().getName()));
                            }

                        }
                    }
                } else {
                    Exception exception = task.getException();
                    if (exception instanceof ApiException) {
                        ApiException apiException = (ApiException) exception;
                        Log.e(LOG, "[LOG] --> Place not found: " + apiException.getStatusCode());
                    }
                }
            });
        }
    }

    public void setMapWithMarkers(Context activityContext, SupportMapFragment supportMapFragment, String apiKey) throws SecurityException {

        if(supportMapFragment != null) {

//            supportMapFragment.getMapAsync(googleMap -> {
//                FusedLocationProviderClient fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(activityContext.getApplicationContext());
//                try {
//                    fusedLocationProviderClient.getLastLocation()
//                            .addOnSuccessListener(location -> {
//                                if(location != null) {
//
//                                    // SET CURRENT POSITION MARKER
//                                    Log.d(LOG, "[LOG] --> Add marker to current position !");
//
//                                    // Display my location
//                                    googleMap.setMyLocationEnabled(true);
//
//                                    // Display location button for zoom in on my current position
//                                    googleMap.getUiSettings().setMyLocationButtonEnabled(true);
//
//
//                                    // SET MARKERS
//                                    Log.d(LOG, "[LOG] --> Add marker for each place !");
//                                    Places.initialize(activityContext.getApplicationContext(), apiKey);
//                                    PlacesClient placesClient = Places.createClient(activityContext.getApplicationContext());
//
//                                    // Use fields to define the data types to return.
//                                    List<Place.Field> placeFields = Arrays.asList(Place.Field.ID, Place.Field.NAME, Place.Field.LAT_LNG);
//
//                                    // Use the builder to create a FindCurrentPlaceRequest.
//                                    FindCurrentPlaceRequest request = FindCurrentPlaceRequest.newInstance(placeFields);
//
//                                    Task<FindCurrentPlaceResponse> placeResponse = placesClient.findCurrentPlace(request);
//                                    placeResponse.addOnCompleteListener(task -> {
//                                        if (task.isSuccessful()){
//                                            FindCurrentPlaceResponse response = task.getResult();
//                                            for (PlaceLikelihood placeLikelihood : response.getPlaceLikelihoods()) {
//                                                if(placeLikelihood.getPlace().getLatLng() != null) {
//                                                    googleMap.addMarker(new MarkerOptions().position(placeLikelihood.getPlace().getLatLng()).title(placeLikelihood.getPlace().getName()));
//                                                }
//                                            }
//                                        } else {
//                                            Exception exception = task.getException();
//                                            if (exception instanceof ApiException) {
//                                                ApiException apiException = (ApiException) exception;
//                                                Log.e(LOG, "[LOG] --> Place not found: " + apiException.getStatusCode());
//                                            }
//                                        }
//                                    });
//                                }
//                            })
//                            .addOnFailureListener(e -> Log.e(LOG, "[LOG] --> " + e));
//                } catch (SecurityException e) {
//                    Log.e(LOG, "[LOG] --> Permission Denied : " + e);
//                }
//            });

        }

    }
}
