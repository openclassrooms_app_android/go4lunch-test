package com.valentin.go4lunch.utils;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.location.LocationManager;
import android.util.Log;

import com.google.android.gms.common.api.ResolvableApiException;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResponse;
import com.google.android.gms.location.LocationSettingsStates;
import com.google.android.gms.tasks.Task;
import com.valentin.go4lunch.ui.main.MainActivity;

import androidx.core.app.ActivityCompat;

public class LocationHelper {

    private static final String LOG = LocationHelper.class.getSimpleName();
    private final Context activityContext;

    private ResolvableApiException resolvableApiException;

    public LocationHelper(Context activityContext) {
        this.activityContext = activityContext;
    }

    public boolean checkLocationPermission() {
        return (ActivityCompat.checkSelfPermission(activityContext.getApplicationContext(), Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(activityContext.getApplicationContext(), Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED);
    }

    public boolean isGPSEnabled(LocationManager locationManager) {
        boolean isEnabled;

        isEnabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        return isEnabled;

    }

    public void turnOnGPS() {

        LocationRequest locationRequest = LocationRequest.create();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setExpirationDuration(2000);

        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(locationRequest);
        builder.setAlwaysShow(true);

        Task<LocationSettingsResponse> result = LocationServices.getSettingsClient(activityContext.getApplicationContext())
                .checkLocationSettings(builder.build());

        result.addOnSuccessListener(locationSettingsResponse -> {
            LocationSettingsStates states = locationSettingsResponse.getLocationSettingsStates();
            if(states != null && states.isLocationPresent()) {
                Log.d(LOG, "[LOG] --> GPS is already turned on ! !");
            }
        });

        result.addOnFailureListener(e -> {
            if(e instanceof ResolvableApiException) {
                try {
                    if(resolvableApiException == null) {
                        resolvableApiException = (ResolvableApiException) e;
                    }

                    resolvableApiException.startResolutionForResult(((Activity) activityContext), MainActivity.REQUEST_CHECK_SETTINGS);
                } catch(IntentSender.SendIntentException ex) {
                    ex.printStackTrace();
                }
            }
        });
    }

}
