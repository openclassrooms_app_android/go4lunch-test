package com.valentin.go4lunch.utils;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.util.Log;

import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.Task;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.api.model.PlaceLikelihood;
import com.google.android.libraries.places.api.net.FindCurrentPlaceRequest;
import com.google.android.libraries.places.api.net.FindCurrentPlaceResponse;
import com.google.android.libraries.places.api.net.PlacesClient;

import java.util.Arrays;
import java.util.List;

import androidx.annotation.IdRes;
import androidx.core.content.ContextCompat;

public class MarkerHelper {

    private static final String LOG = MarkerHelper.class.getSimpleName();
    private final Context activityContext;
    private final Resources resources;

    public MarkerHelper(Context activityContext, Resources resources) {
        this.activityContext = activityContext;
        this.resources = resources;
    }

    public BitmapDescriptor bitmapFromVector(Context context, int vectorResId) {
        // below line is use to generate a drawable.
        Drawable vectorDrawable = ContextCompat.getDrawable(context, vectorResId);
        Bitmap bitmap = null;

        if(vectorDrawable != null) {
            // below line is use to set bounds to our vector drawable.
            vectorDrawable.setBounds(0, 0, vectorDrawable.getIntrinsicWidth(), vectorDrawable.getIntrinsicHeight());

            // below line is use to create a bitmap for our
            // drawable which we have added.
            bitmap = Bitmap.createBitmap(vectorDrawable.getIntrinsicWidth(), vectorDrawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        }


        // below line is use to add bitmap in our canvas.
        Canvas canvas = new Canvas(bitmap);

        if(vectorDrawable != null) {
            // below line is use to draw our
            // vector drawable in canvas.
            vectorDrawable.draw(canvas);
        }

        // after generating our bitmap we are returning our bitmap.
        BitmapDescriptor bitmapDescriptor = null;
        if(bitmap != null) {
            bitmapDescriptor = BitmapDescriptorFactory.fromBitmap(bitmap);
        }

        return bitmapDescriptor;
    }

    public Bitmap resizeBitmap(@IdRes int drawableInt, int width, int height){
        Bitmap imageBitmap = BitmapFactory.decodeResource(resources, drawableInt);
        return Bitmap.createScaledBitmap(imageBitmap, width, height, false);
    }

    public boolean checkLocationPermission() {
        return (ContextCompat.checkSelfPermission(activityContext, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED);
    }

    public void setMarkers(GoogleMap map, int drawableInt, String apiKey) {
        Places.initialize(activityContext.getApplicationContext(), apiKey);
        PlacesClient placesClient = Places.createClient(activityContext.getApplicationContext());

        // Use fields to define the data types to return.
        List<Place.Field> placeFields = Arrays.asList(Place.Field.ID, Place.Field.NAME, Place.Field.LAT_LNG);

        // Use the builder to create a FindCurrentPlaceRequest.
        FindCurrentPlaceRequest request = FindCurrentPlaceRequest.newInstance(placeFields);

        if(checkLocationPermission()) {
            Task<FindCurrentPlaceResponse> placeResponse = placesClient.findCurrentPlace(request);
            placeResponse.addOnCompleteListener(task -> {
                if (task.isSuccessful()){
                    FindCurrentPlaceResponse response = task.getResult();
                    for (PlaceLikelihood placeLikelihood : response.getPlaceLikelihoods()) {
                        if(placeLikelihood.getPlace().getLatLng() != null) {

                            if(drawableInt != 0) {
                                BitmapDescriptor marker = BitmapDescriptorFactory.fromBitmap(this.resizeBitmap(drawableInt,90,90));
                                map.addMarker(new MarkerOptions().position(placeLikelihood.getPlace().getLatLng()).title(placeLikelihood.getPlace().getName()).icon(marker));
                            } else {
                                map.addMarker(new MarkerOptions().position(placeLikelihood.getPlace().getLatLng()).title(placeLikelihood.getPlace().getName()));
                            }

                        }
                    }
                } else {
                    Exception exception = task.getException();
                    if (exception instanceof ApiException) {
                        ApiException apiException = (ApiException) exception;
                        Log.e(LOG, "[LOG] --> Place not found: " + apiException.getStatusCode());
                    }
                }
            });
        }
    }

}
