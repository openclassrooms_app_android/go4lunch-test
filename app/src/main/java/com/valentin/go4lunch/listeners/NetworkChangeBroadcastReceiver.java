package com.valentin.go4lunch.listeners;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

import com.valentin.go4lunch.utils.NetworkConnectivity;

public class NetworkChangeBroadcastReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        if(!NetworkConnectivity.hasActiveInternetConnection(context)) {
            Toast.makeText(context, "Internet non disponible !", Toast.LENGTH_SHORT).show();
        }
    }

}
