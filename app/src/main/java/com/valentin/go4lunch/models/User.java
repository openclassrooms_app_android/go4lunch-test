package com.valentin.go4lunch.models;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "users")
public class User implements Parcelable {

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id")
    private long id;

    @ColumnInfo(name = "uid")
    private String uid;

    @ColumnInfo(name = "avatar")
    private String avatar;

    @ColumnInfo(name = "displayName")
    private String displayName;

    @ColumnInfo(name = "email")
    private String email;

    /**
     * Constructor
     * @param uid
     * @param avatar
     * @param displayName
     * @param email
     */
    public User(String uid, String avatar, String displayName, String email) {
        this.uid = uid;
        this.avatar = avatar;
        this.displayName = displayName;
        this.email = email;
    }

    public User() {

    }

    protected User(Parcel in) {
        id = in.readLong();
        uid = in.readString();
        avatar = in.readString();
        displayName = in.readString();
        email = in.readString();
    }

    public static final Creator<User> CREATOR = new Creator<User>() {
        @Override
        public User createFromParcel(Parcel in) {
            return new User(in);
        }

        @Override
        public User[] newArray(int size) {
            return new User[size];
        }
    };

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeLong(id);
        parcel.writeString(uid);
        parcel.writeString(avatar);
        parcel.writeString(displayName);
        parcel.writeString(email);
    }
}
