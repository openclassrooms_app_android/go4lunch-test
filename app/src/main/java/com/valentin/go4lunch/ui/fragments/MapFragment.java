package com.valentin.go4lunch.ui.fragments;

import android.Manifest;
import android.content.pm.PackageManager;
import android.location.LocationManager;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.valentin.go4lunch.R;
import com.valentin.go4lunch.ui.main.MainActivity;
import com.valentin.go4lunch.utils.LocationHelper;
import com.valentin.go4lunch.utils.MapStateManager;
import com.valentin.go4lunch.utils.MarkerHelper;
import com.valentin.go4lunch.utils.PreferencesHelper;

import static android.content.Context.LOCATION_SERVICE;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link MapFragment#getInstance} factory method to
 * create an instance of this fragment.
 */
public class MapFragment extends Fragment implements OnMapReadyCallback {

    private static final String LOG = MapFragment.class.getSimpleName();

    private static final float DEFAULT_ZOOM = 17f;
    private static final int ANIMATION_CAMERA_DURATION = 1000;
    private static final String ANIMATION_HAS_BEEN_EXECUTED = "ANIMATION_HAS_BEEN_EXECUTED";

    private GoogleMap mMap;
    private boolean animationHasBeenExecuted;
    private ImageView locationButton;

    private static MapFragment INSTANCE;

    public MapFragment() {
        // Required empty public constructor
    }


    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment MapFragment.MapFragment
     */
    public static MapFragment getInstance() {
        if(INSTANCE == null) {
            INSTANCE = new MapFragment();
        }
        return INSTANCE;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(LOG, "[LOG] --> onCreate !");

        if(savedInstanceState == null) {
            animationHasBeenExecuted = false;
        } else {
            animationHasBeenExecuted = savedInstanceState.getBoolean(ANIMATION_HAS_BEEN_EXECUTED);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_map, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        locationButton = view.findViewById(R.id.location_button_fragment_map);
    }

    @Override
    public void onMapReady(@NonNull GoogleMap googleMap) {
        Log.d(LOG, "[LOG] --> onMapReady !");
        mMap = googleMap;

        this.configureMap();

        locationButton.setOnClickListener(view -> {
            double lat = 0;
            double lng = 0;
            if(getActivity() != null) {
                lat = Double.parseDouble(PreferencesHelper.getSharedPreferenceString(getActivity().getBaseContext(), MainActivity.CURRENT_LOCATION_LATITUDE, null));
                lng = Double.parseDouble(PreferencesHelper.getSharedPreferenceString(getActivity().getBaseContext(), MainActivity.CURRENT_LOCATION_LONGITUDE, null));
            }
            LatLng latLng = new LatLng(lat, lng);
            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, DEFAULT_ZOOM), ANIMATION_CAMERA_DURATION, null);
        });

        if(!animationHasBeenExecuted) {
            this.animateCameraOnMyCurrentPositionAndDisplayAllMarkers();
            animationHasBeenExecuted = true;
        } else {
            this.moveCameraOnMyLastKnownPositionAndDisplayAllMarkers();
        }
    }

    /**
     * Lifecycle Fragment
     */
    @Override
    public void onPause() {
        super.onPause();
        Log.d(LOG, "[LOG] --> onPause !");
        if(getActivity() != null) {
            MapStateManager mapStateManager = new MapStateManager(getActivity().getApplicationContext());
            mapStateManager.saveMapState(mMap);
            Toast.makeText(getActivity().getApplicationContext(), "Map State has been save", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.d(LOG, "[LOG] --> onResume !");
        this.setupMapIfNeeded();

        if(mMap != null && getActivity() != null) {
            if(MainActivity.checkLocationPermission(getActivity().getBaseContext())) {
                mMap.setMyLocationEnabled(true);
                this.displayMarkers(mMap);
            }
        }
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putBoolean(ANIMATION_HAS_BEEN_EXECUTED, animationHasBeenExecuted);
    }

    /**
     * Custom Methods
     */

    private void setupMapIfNeeded() {
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        if (mMap == null) {
            SupportMapFragment mapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.fragment_container_fragment_map);
            if (mapFragment != null) {
                mapFragment.getMapAsync(this);
            }
        }
    }

    private void configureMap() {
        if(getActivity() != null) {
            // Set new map style
            mMap.setMapStyle(MapStyleOptions.loadRawResourceStyle(
                    getActivity().getApplicationContext(), R.raw.standard_map_json));
        }

        if(MainActivity.checkLocationPermission(getActivity().getBaseContext())) {
            // Display my current location
            mMap.setMyLocationEnabled(true);
        }

        // Hide location button for zoom in on my current position
        mMap.getUiSettings().setMyLocationButtonEnabled(false);
    }

    private void animateCameraOnMyCurrentPositionAndDisplayAllMarkers() {
        mMap.setOnMapLoadedCallback(() -> {
            if(getActivity() != null) {
                if(MainActivity.checkLocationPermission(getActivity().getBaseContext())) {
                    double lat = 0;
                    double lng = 0;
                    if(getActivity() != null) {
                        lat = Double.parseDouble(PreferencesHelper.getSharedPreferenceString(getActivity().getBaseContext(), MainActivity.CURRENT_LOCATION_LATITUDE, null));
                        lng = Double.parseDouble(PreferencesHelper.getSharedPreferenceString(getActivity().getBaseContext(), MainActivity.CURRENT_LOCATION_LONGITUDE, null));
                    }
                    mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(lat, lng), DEFAULT_ZOOM), ANIMATION_CAMERA_DURATION, null);
                }
            }

            // Display all restaurants markers
            this.displayMarkers(mMap);
        });
    }

    private void moveCameraOnMyLastKnownPositionAndDisplayAllMarkers() {
        if(getActivity() != null) {
            MapStateManager mapStateManager = new MapStateManager(getActivity().getApplicationContext());

            CameraPosition position = mapStateManager.getSavedCameraPosition();
            if(position != null) {
                CameraUpdate update = CameraUpdateFactory.newCameraPosition(position);
                Toast.makeText(getActivity().getApplicationContext(), "entering Resume State", Toast.LENGTH_SHORT).show();
                mMap.moveCamera(update);
                mMap.setMapType(mapStateManager.getSavedMapType());
                if(MainActivity.checkLocationPermission(getActivity().getBaseContext())) {
                    mMap.setMyLocationEnabled(true);
                }
            }
        }

        // Display all restaurants markers
         this.displayMarkers(mMap);
    }

    public void displayMarkers(GoogleMap map) {
        MarkerHelper markerHelper = new MarkerHelper(getActivity(), getResources());
        markerHelper.setMarkers(map, R.drawable.marker_place, getResources().getString(R.string.google_api_key));
    }
}