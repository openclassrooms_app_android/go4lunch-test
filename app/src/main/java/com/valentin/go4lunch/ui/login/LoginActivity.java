package com.valentin.go4lunch.ui.login;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.util.Log;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.lifecycle.ViewModelProvider;

import com.firebase.ui.auth.AuthMethodPickerLayout;
import com.firebase.ui.auth.AuthUI;
import com.firebase.ui.auth.IdpResponse;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.valentin.go4lunch.R;
import com.valentin.go4lunch.injections.Injection;
import com.valentin.go4lunch.injections.ViewModelFactory;
import com.valentin.go4lunch.models.User;
import com.valentin.go4lunch.ui.main.MainActivity;
import com.valentin.go4lunch.viewmodels.UserViewModel;

import java.util.Arrays;
import java.util.List;

public class LoginActivity extends AppCompatActivity {

    private final String LOG = LoginActivity.class.getSimpleName();

    public static final String UID = "uid";
    private static final int RC_SIGN_IN = 123;
    private UserViewModel userViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        this.configureViewModel();
        this.createSignInIntent();
    }

    private void configureViewModel(){
        ViewModelFactory mViewModelFactory = Injection.provideViewModelFactory(LoginActivity.this);
        this.userViewModel = new ViewModelProvider(this, mViewModelFactory).get(UserViewModel.class);
    }

    public void createSignInIntent() {
        // Choose authentication providers
        List<AuthUI.IdpConfig> providers = Arrays.asList(
                new AuthUI.IdpConfig.FacebookBuilder().build(),
                new AuthUI.IdpConfig.GoogleBuilder().build());

        AuthMethodPickerLayout customLayout = new AuthMethodPickerLayout
                .Builder(R.layout.custom_login)
                .setFacebookButtonId(R.id.custom_login_facebook_button)
                .setGoogleButtonId(R.id.custom_login_google_button)
                .build();

        startActivityForResult(AuthUI.getInstance()
                        .createSignInIntentBuilder()
                        .setAvailableProviders(providers)
                        .setIsSmartLockEnabled(true, false)
                        .setAuthMethodPickerLayout(customLayout)
                        .build(), RC_SIGN_IN);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RC_SIGN_IN && data != null) {

            IdpResponse response = IdpResponse.fromResultIntent(data);
            if (resultCode == RESULT_OK) {
                // Successfully signed in
                Log.d(LOG, "[LOG] --> Connexion établie !");

                FirebaseUser userAuth = FirebaseAuth.getInstance().getCurrentUser();
                if(userAuth != null) {
                    goToNextActivity(userAuth);
                }
            } else {
                // Sign in failed. If response is null the user canceled the
                // sign-in flow using the back button. Otherwise check
                // response.getError().getErrorCode() and handle the error.
                Log.d(LOG, "[LOG] --> Connexion impossible !");
                if(response != null) {
                    Log.e(LOG, "[LOG] --> " + response.getError());
                }
            }

        }

    }

    public void goToNextActivity(FirebaseUser userAuth) {
        User user = new User(userAuth.getUid(), (userAuth.getPhotoUrl() != null ? userAuth.getPhotoUrl().toString() : null), userAuth.getDisplayName(), userAuth.getEmail());
        userViewModel.createUser(user);

        Intent goToNextActivity;
        goToNextActivity = new Intent(this, MainActivity.class);
        goToNextActivity.putExtra(LoginActivity.UID, userAuth.getUid());
        startActivity(goToNextActivity);
    }

}
