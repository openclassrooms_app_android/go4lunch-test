package com.valentin.go4lunch.injections;

import android.content.Context;

import com.valentin.go4lunch.services.UserService;

import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

public class Injection {

    public static UserService provideUserServiceSource(Context context) {
        return UserService.getInstance(context);
    }

    public static Executor provideExecutor(){ return Executors.newSingleThreadExecutor(); }

    public static ViewModelFactory provideViewModelFactory(Context context) {
        UserService userServiceSource = provideUserServiceSource(context);
        Executor executor = provideExecutor();
        return new ViewModelFactory(userServiceSource, executor);
    }
}
