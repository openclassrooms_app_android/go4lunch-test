package com.valentin.go4lunch.injections;

import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import com.valentin.go4lunch.services.UserService;
import com.valentin.go4lunch.viewmodels.UserViewModel;

import java.util.concurrent.Executor;

public class ViewModelFactory implements ViewModelProvider.Factory {

    private final UserService userService;
    private final Executor executor;

    public ViewModelFactory(UserService userService, Executor executor) {
        this.userService = userService;
        this.executor = executor;
    }

    @NonNull
    @Override
    public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
        if(modelClass.isAssignableFrom(UserViewModel.class)) {
            return (T) new UserViewModel(userService, executor);
        }
        throw new IllegalArgumentException("Unknown ViewModel class");
    }
}
